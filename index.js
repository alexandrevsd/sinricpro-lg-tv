require('dotenv').config();
const { appKey, deviceId, secretKey } = require("./credentials");
const { SinricPro, SinricProActions } = require('sinricpro');
const wol = require('wake_on_lan');
const lgTv = require("lgtv2")({url: 'ws://' + process.env.IP + ':3000'});

lgTv.on('error', function (err) {
    console.log(err);
});

lgTv.on('connect', function () {
    console.log('connected');
});

function sendToast(message) {
    lgTv.request('ssap://system.notifications/createToast', {message});
}

function setPowerState(deviceId, data) {
    if(data === 'Off') {
        lgTv.request('ssap://system/turnOff');
    } else if(data === 'On') {
        wol.wake(process.env.MAC, {address: process.env.IP});
    }
    return true;
}

function setVolume(deviceId, volume) {
    lgTv.request('ssap://audio/setVolume', {volume}, function(err, res) {
        if(err) {
            sendToast('Error while changing volume');
            console.error(err);
        }
    })
    return true;
}

function adjustVolume(deviceId, volume) {
    setVolume(deviceId, volume);
    return true;
}

function setMute(deviceId, data) {
    const mute = !!data;
    lgTv.request('ssap://audio/setMute', {mute}, function(err, res) {
        if(err) {
            sendToast('Error while setting mute');
            console.error(err);
        }
    })
    return true;
}

function selectInput(deviceId, data) {
    const inputId = data.replace(' ', '_');
    lgTv.request('ssap://tv/switchInput', {inputId},function (err, res) {
        if(err) {
            sendToast('Error while changing input');
            console.error(err);
        }
        if(res.errorText && res.errorText === 'no such input') {
            sendToast('Error for changing input to "' + inputId + '"');
        }
    });
    return true;
}

const callbacks = {
    setPowerState,
    setVolume,
    adjustVolume,
    setMute,
    selectInput
};

const sinricPro = new SinricPro(appKey, deviceId, secretKey, true);
SinricProActions(sinricPro, callbacks);