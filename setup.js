const {toMAC} = require("@network-utils/arp-lookup");
const fs = require('fs');

async function setup() {

    const ip = '192.168.1.99';
    const mac = (await toMAC(ip)).toUpperCase();
    const data = `IP=${ip}
MAC=${mac}`;
    fs.writeFileSync('.env', data);

}

setup();